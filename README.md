
## Available Scripts

In the project directory, you should run:

### `npm install`

This command installs all the needed packages. You should run it before the next command

### `node app.js`

Runs the server in localhost - on port 8081