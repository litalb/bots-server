const BotsDB = require("./BotsDBRepository.js");
const BotInitializer = require("./BotInitializer");
const express = require('express');
const cors = require('cors')
const app = express();


app.use(cors());

BotsDB.getAllBots(bots =>
    bots.forEach(bot => BotInitializer.initializeBot(bot)));

app.get('/getBotsInfo', function (req, res) {
    BotsDB.getAllBots(docs => {
        res.end(JSON.stringify(docs));
    });
});

const server = app.listen(8081, function () {
    console.log("Server is up");
});