const mongojs = require("mongojs");
const assert = require("assert");
const db = mongojs("mongodb://zencity_bots:bot123@ds145325.mlab.com:45325/datahack", ["bots"]);

function getAllBots(callback){
    db.bots.find(function (err, docs) {
        assert.strictEqual(err, null);
        callback(docs);
    });
}

function getBotStatus(botId, callback){
    db.bots.findOne(
        {_id: mongojs.ObjectId(botId)},
        function (err, doc) {
            assert.strictEqual(err, null);
            callback(doc.status);
    });
}

function updateBotStatus(botId, status, callback){
    db.bots.update(
        {_id: mongojs.ObjectId(botId)},
        {$set: {status: status}},
        function (err) {
        assert.strictEqual(err, null);
        callback();
    });
}

module.exports = {
    getAllBots: getAllBots,
    getBotStatus: getBotStatus,
    updateBotStatus: updateBotStatus,
};

// Create a function to terminate your app gracefully:
function closeMongo(){
    db.close(true, () => {
        console.log("closed Mongo connection");
        process.exit();
    });
}

process.on('exit', closeMongo);
process.on('SIGINT', closeMongo);
process.on('SIGTERM', closeMongo);
process.on('SIGKILL', closeMongo);
process.on('uncaughtException', closeMongo);
