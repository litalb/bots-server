const TeleBot = require('telebot');
const BotsDB = require("./BotsDBRepository.js");

const tokens = {
    customersupbot1: "419831156:AAGGe0b1pu7l1nMXKr9FIsMCFN16T9Jy-9g",
    customersupbot2: "429977475:AAGJ_nd4kWvLkQrX8Nvb3IQ-wyxOH5GovqY",
    customersupbot3: "432976860:AAE_51EwJhuVAVweSRuhEUuQnN2dVcnTvKE",
};

const BotStatus = {
    FREE: "free",
    BUSY: "busy",
};

const engagedUsers = {};

const commandsInfo = "/start - displays all supported commands\n" +
    "/engage - starts engaging with the bot\n" +
    "/askforhelp <topic> - asks the bot a question\n" +
    "/disengage- frees the bot to engage other people";

const initializeBot = botInfo => {
    const botToken = tokens[botInfo.name];
    if(typeof botToken === 'undefined'){
        return;
    }

    const bot = new TeleBot(botToken);
    bot.on("/start", msg => msg.reply.text(commandsInfo));
    bot.on("/engage", msg => handleEngage(botInfo._id, msg));
    bot.on("/askforhelp", msg => handleAskForHelp(botInfo._id, botInfo.answer, msg));
    bot.on("/disengage", msg => handleDisengage(botInfo._id, msg));
    bot.start();
};

const handleEngage = (botID, msg) => {
    BotsDB.getBotStatus(botID, status => {
        if(status === BotStatus.FREE){
            BotsDB.updateBotStatus(botID, BotStatus.BUSY, () => {
                    engagedUsers[botID] = msg.from.id;
                    msg.reply.text("I'm ready for your questions");
                });
        } else {
            msg.reply.video("https://media.giphy.com/media/cOFLK7ZbliXW21RfmE/giphy.gif")
        }
    });
};

const handleDisengage = (botID, msg) => {
    BotsDB.getBotStatus(botID, status => {
        if(status === BotStatus.BUSY){
            BotsDB.updateBotStatus(botID, BotStatus.FREE, () => {
                engagedUsers[botID] = null;
                msg.reply.video("https://tenor.com/view/genie-aladdin-free-happiness-im-free-gif-5220219");
            });
        } else {
            msg.reply.text("I'm already free");
        }
    });
};

const handleAskForHelp = (botID, answer, msg) => {
    BotsDB.getBotStatus(botID, status => {
        if(engagedUsers[botID] === msg.from.id){
            msg.reply.text(answer);
        } else {
            msg.reply.text("If you wanna ask me questions, you gotta engage with me first");
        }
    });
};

module.exports= {
    initializeBot: initializeBot
};